package com.innpolis.testtweeter.controller;

import com.innpolis.testtweeter.TestTweeterApplicationTests;
import com.innpolis.testtweeter.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class UserControllerTest extends TestTweeterApplicationTests {
    
    @Test
    void testGetAllUsers() {
        ResponseEntity<List<User>> usersListResponse = restTemplate.exchange(baseUrl + "/users",
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
        });
        
        List<User> usersList = usersListResponse.getBody();
    
        assertEquals(HttpStatus.OK, usersListResponse.getStatusCode(),
                "Status code for the get all users request should be 200");
        
        assertNotNull(usersList);
    }
    
    @Test
    void getById() {
    }
    
    @Test
    void createUser() {
    }
    
    @Test
    void updateUser() {
    }
    
    @Test
    void deleteUser() {
    }
}