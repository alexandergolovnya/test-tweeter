package com.innpolis.testtweeter;

import com.innpolis.testtweeter.controller.UserController;
import com.innpolis.testtweeter.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestTweeterApplicationTests {
    
    @LocalServerPort
    protected int port;
    
    protected String baseUrl = "http://localhost:";
    protected RestTemplate restTemplate = new RestTemplate();
    
    @BeforeEach
    public void setUp() {
        baseUrl = baseUrl.concat(port + "");
    }
    
    @Test
    void contextLoads() {
    }
    
    protected User createTestUser() {
        return User.builder()
                .firstName("Aleksei")
                .lastName("Pavlosky")
                .email("alekseipavlosky@gmail.com")
                .phone("+2564556667788")
                .nickname("alekseipavlosky")
                .build();
    }
}
