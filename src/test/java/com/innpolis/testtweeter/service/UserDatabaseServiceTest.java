package com.innpolis.testtweeter.service;

import com.innpolis.testtweeter.TestTweeterApplicationTests;
import com.innpolis.testtweeter.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserDatabaseServiceTest extends TestTweeterApplicationTests {
    
    @Autowired
    private UserDatabaseService service;
    
    @Test
    void testGetById() {
        User createdUser = service.createUser(createTestUser());
    
        User user = service.getById(createdUser.getId());
    
        assertNotNull(user);
        assertEquals(user.getId(), createdUser.getId());
        assertEquals(user.getFirstName(), createdUser.getFirstName());
        assertEquals(user.getLastName(), createdUser.getLastName());
        assertEquals(user.getEmail(), createdUser.getEmail());
        assertEquals(user.getPhone(), createdUser.getPhone());
        assertEquals(user.getNickname(), createdUser.getNickname());
    }
    
    @Test
    void testGetByNonExistingId() {
        UUID uuid = UUID.randomUUID();
    
        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(uuid),
                format("Expected to return nothing and throw exception, since object with generated id %s doesn't exist", uuid)
        );
        
        assertEquals(format("404 NOT_FOUND \"User with id %s not found\"", uuid), exception.getMessage());
    }
    
    @Test
    void testGetAll() {
        List<User> usersList = service.getAll();
        assertNotNull(usersList);
    }
    
    @Test
    void testCreateUser() {
        User user = createTestUser();
    
        User createdUser = service.createUser(user);
        
        assertNotNull(createdUser.getId());
        assertEquals(user.getFirstName(), createdUser.getFirstName());
        assertEquals(user.getLastName(), createdUser.getLastName());
        assertEquals(user.getEmail(), createdUser.getEmail());
        assertEquals(user.getPhone(), createdUser.getPhone());
        assertEquals(user.getNickname(), createdUser.getNickname());
    }
    
    @Test
    void testUpdateUser() {
        User createdUser = service.createUser(createTestUser());

        User userToUpdate = new User(createdUser).toBuilder()
                .id(UUID.randomUUID())
                .firstName("Igor")
                .lastName("Kolesnik")
                .email("igorkolesnik@gmail.com")
                .phone("+14556667788")
                .nickname("igorkolesnik")
                .build();
    
        User updatedUser = service.updateUser(createdUser.getId(), userToUpdate);
    
        assertEquals(createdUser.getId(), updatedUser.getId(), "Ids shouldn't be changed during the update");
        assertNotEquals(updatedUser.getFirstName(), createdUser.getFirstName());
        assertNotEquals(updatedUser.getLastName(), createdUser.getLastName());
        assertNotEquals(updatedUser.getEmail(), createdUser.getEmail());
        assertNotEquals(updatedUser.getPhone(), createdUser.getPhone());
        assertNotEquals(updatedUser.getNickname(), createdUser.getNickname());
    }
    
    @Test
    void testDelete() {
        User createdUser = service.createUser(createTestUser());
        UUID createdUserId = createdUser.getId();
        service.delete(createdUserId);
    
        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(createdUserId),
                format("Expected to return nothing and throw exception, since user with id %s was deleted",
                        createdUserId)
        );
    
        assertEquals(format("404 NOT_FOUND \"User with id %s not found\"", createdUserId), exception.getMessage());
    
    }
}