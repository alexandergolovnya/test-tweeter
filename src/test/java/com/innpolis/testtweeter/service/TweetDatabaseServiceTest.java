package com.innpolis.testtweeter.service;

import com.innpolis.testtweeter.TestTweeterApplicationTests;
import com.innpolis.testtweeter.model.Tweet;
import com.innpolis.testtweeter.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class TweetDatabaseServiceTest extends TestTweeterApplicationTests {
    
    @Autowired
    private TweetDatabaseService service;
    
    @Autowired
    private UserDatabaseService userDatabaseService;
    
    private User testUser;
    
    @BeforeEach
    public void init() {
        testUser = userDatabaseService.createUser(createTestUser());
    }
    
    @Test
    void getById() {
    }
    
    @Test
    void getAll() {
    }
    
    @Test
    void testCreateTweet() {
        Tweet testTweet = createTestTweet();
    
        Tweet createdTweet = service.createTweet(testTweet);
        
        assertNotNull(createdTweet);
        assertNotNull(createdTweet.getUser());
        assertNotNull(createdTweet.getId());
        
        assertEquals(testTweet.getContent(), createdTweet.getContent());
        assertEquals(testTweet.getUser(), createdTweet.getUser());
    }
    
    @Test
    void updateTweet() {
    }
    
    @Test
    void delete() {
    }
    
    private Tweet createTestTweet() {
        return Tweet.builder()
                .content("Test tweet from Aleksei")
                .user(testUser)
                .build();
    }
}