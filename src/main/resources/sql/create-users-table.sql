CREATE TABLE users (
    id UUID PRIMARY KEY,
    nickname varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    phone varchar(255),
    firstName varchar(255),
    lastName varchar(255)
)