package com.innpolis.testtweeter.repository;

import com.innpolis.testtweeter.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

@Repository
public interface UserRepository extends CrudRepository<User, UUID> {
    List<User> findAllByEmailIsLike(String term);
//    List<User> findAllByEmailIsLikeOrNicknameLikeOrFirstNameLikeOrLastNameLikeOrPhoneLike(String term);
}
