package com.innpolis.testtweeter.repository;

import com.innpolis.testtweeter.model.Tweet;
import com.innpolis.testtweeter.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TweetRepository extends CrudRepository<Tweet, UUID> {
}
