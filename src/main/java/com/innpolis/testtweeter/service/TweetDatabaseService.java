package com.innpolis.testtweeter.service;

import com.innpolis.testtweeter.model.Tweet;
import com.innpolis.testtweeter.model.User;
import com.innpolis.testtweeter.repository.TweetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;


@Service
@RequiredArgsConstructor
public class TweetDatabaseService implements TweetService {
    
    private final TweetRepository tweetRepository;
    
    @Override
    public Tweet getById(UUID id) {
        return tweetRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Tweet with id %s not found", id)));
    }
    
    @Override
    public List<Tweet> getAll() {
        Iterable<Tweet> tweetsIterable = tweetRepository.findAll();
        List<Tweet> tweetsList = new ArrayList<>();
        tweetsIterable.forEach(tweetsList::add);
        return tweetsList;
    }
    
    @Override
    public Tweet createTweet(Tweet tweet) {
        tweet.setId(UUID.randomUUID());
        return tweetRepository.save(tweet);
    }
    
    @Override
    public Tweet updateTweet(UUID id, Tweet tweet) {
        Tweet tweetFromDatabase = getById(id);
    
        Tweet tweetUpdate = tweetFromDatabase.toBuilder()
                .content(tweet.getContent())
                .build();
    
        return tweetRepository.save(tweetUpdate);
    }
    
    @Override
    public void delete(UUID id) {
        tweetRepository.deleteById(id);
    }
}
