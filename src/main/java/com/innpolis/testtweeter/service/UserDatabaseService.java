package com.innpolis.testtweeter.service;

import com.innpolis.testtweeter.model.User;
import com.innpolis.testtweeter.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class UserDatabaseService implements UserService {
    
    private final UserRepository userRepository;
    
    @PostConstruct
    public void init() {
        long usersCount = userRepository.count();
        
        if (usersCount < 10) {
            for (int i = 1; i <= 10; ++i) {
                User user = User.builder()
                        .id(UUID.randomUUID())
                        .lastName("Last name " + i)
                        .firstName("First name " + i)
                        .nickname("Nickname name " + i)
                        .phone("+79995556688")
                        .email("test@test.com")
                        .build();
        
                userRepository.save(user);
            }
        }
    }
    
    @Override
    public User getById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("User with id %s not found", id)));
    }
    
    @Override
    public List<User> getAll() {
        Iterable<User> usersIterable = userRepository.findAll();
        List<User> usersList = new ArrayList<>();
        usersIterable.forEach(usersList::add);
        return usersList;
    }
    
    @Override
    public User createUser(User user) {
        user.setId(UUID.randomUUID());
        return userRepository.save(user);
    }
    
    @Override
    public User updateUser(UUID id, User user) {
        User userFromDatabase = getById(id);
        
        User userToUpdate = userFromDatabase.toBuilder()
                .email(user.getEmail())
                .phone(user.getPhone())
                .nickname(user.getNickname())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
        
        return userRepository.save(userToUpdate);
    }
    
    @Override
    public void delete(UUID id) {
        userRepository.deleteById(id);
    }
    
    @Override
    public List<User> search(String term) {
        return userRepository.findAllByEmailIsLike(term);
//        return userRepository.findAllByEmailIsLikeOrNicknameLikeOrFirstNameLikeOrLastNameLikeOrPhoneLike(term);
    }
}
