package com.innpolis.testtweeter.controller;

import com.innpolis.testtweeter.model.User;
import com.innpolis.testtweeter.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/users")
@RequiredArgsConstructor
public class UsersFrontendController {
    
    private final UserService userServices;
    
    @GetMapping
    public String usersPage(Model model) {
        List<User> users = userServices.getAll();
        model.addAttribute("users", users);
        return "users/users";
    }
    
    @GetMapping("/{id}")
    public String userPage(Model model, @PathVariable UUID id) {
        User user = userServices.getById(id);
        model.addAttribute("user", user);
        return "users/user";
    }
    
    @GetMapping("/new")
    public String userCreatePage(Model model) {
        model.addAttribute("user", new User());
        return "users/user";
    }
    
    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable UUID id) {
        userServices.delete(id);
        return "redirect:/ui/users";
    }
    
    @PostMapping
    public String createOrUpdateUser(@ModelAttribute("userDto") User userDto) {
        if (userDto.getId() == null) {
            userServices.createUser(userDto);
        } else {
            userServices.updateUser(userDto.getId(), userDto);
        }
        
        return "redirect:/ui/users";
    }
}
