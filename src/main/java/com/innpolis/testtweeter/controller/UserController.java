package com.innpolis.testtweeter.controller;

import com.innpolis.testtweeter.model.User;
import com.innpolis.testtweeter.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    
    private final UserService userServices;
    
    @GetMapping
    public List<User> getAllUsers() {
        return userServices.getAll();
    }
    
    @GetMapping("/search")
    public List<User> getAllUsers(@RequestParam String term) {
        return userServices.search(term);
    }
    
    @GetMapping("/{id}")
    public User getById(@PathVariable UUID id) {
        return userServices.getById(id);
    }
    
    @PostMapping
    public User createUser(@RequestBody User user) {
        return userServices.createUser(user);
    }
    
    @PutMapping("/{id}")
    public User updateUser(@PathVariable UUID id, @RequestBody User user) {
        return userServices.updateUser(id, user);
    }
    
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        userServices.delete(id);
    }
}
