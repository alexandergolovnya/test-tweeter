package com.innpolis.testtweeter.controller;

import com.innpolis.testtweeter.model.Tweet;
import com.innpolis.testtweeter.model.User;
import com.innpolis.testtweeter.service.TweetService;
import com.innpolis.testtweeter.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/tweets")
@RequiredArgsConstructor
public class TweetFrontendController {
    
    private final TweetService tweetService;
    private final UserService userService;
    
    @GetMapping
    public String tweetsPage(Model model) {
        List<Tweet> tweets = tweetService.getAll();
        model.addAttribute("tweets", tweets);
        return "tweets/tweets";
    }
    
    @GetMapping("/{id}")
    public String tweetsPage(Model model, @PathVariable UUID id) {
        Tweet tweet = tweetService.getById(id);
        List<User> users = userService.getAll();
        model.addAttribute("users", users);
        model.addAttribute("tweet", tweet);
        return "tweets/tweet";
    }
    
    @GetMapping("/new")
    public String tweetCreatePage(Model model) {
        List<User> users = userService.getAll();
        model.addAttribute("users", users);
        model.addAttribute("tweet", new Tweet());
        return "tweets/tweet";
    }
    
    @PostMapping("/delete/{id}")
    public String deleteTweet(@PathVariable UUID id) {
        tweetService.delete(id);
        return "redirect:/ui/tweets";
    }
    
    @PostMapping
    public String createOrUpdateTweet(@ModelAttribute("tweetDto") Tweet tweetDto) {
        if (tweetDto.getId() == null) {
            tweetService.createTweet(tweetDto);
        } else {
            tweetService.updateTweet(tweetDto.getId(), tweetDto);
        }
        
        return "redirect:/ui/tweets";
    }
}
