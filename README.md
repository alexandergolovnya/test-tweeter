# Test Twitter Application

## Requirements

### Entities
1. Tweet
2. User
3. Message
4. Blacklist
5. Status

### Functionality
1. CRUD operations for entities
2. Authorization & authentication
3. UI

## Links
1. https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
2. https://en.wikipedia.org/wiki/Universally_unique_identifier#:~:text=A%20universally%20unique%20identifier%20(UUID,%2C%20for%20practical%20purposes%2C%20unique.
3. https://refactoring.guru/design-patterns/builder/java/example
4. https://projectlombok.org/features/Builder
5. https://www.baeldung.com/freemarker-operations
6. https://www.baeldung.com/spring-mvc-form-tutorial
7. https://docs.liquibase.com/change-types/community/add-foreign-key-constraint.html
8. https://stackoverflow.com/questions/24718545/liquibase-how-to-set-foreign-keys-constraint-in-column-tag
9. https://www.vincenzoracca.com/en/blog/framework/spring/integration-test/
10. https://www.testcontainers.org/
11. https://www.baeldung.com/java-copy-constructor